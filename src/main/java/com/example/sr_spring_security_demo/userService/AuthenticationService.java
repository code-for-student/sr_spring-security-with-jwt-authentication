package com.example.sr_spring_security_demo.userService;

import com.example.sr_spring_security_demo.model.AuthenticationRequest;
import com.example.sr_spring_security_demo.model.AuthenticationResponse;
import com.example.sr_spring_security_demo.repository.UserRepository;
import com.example.sr_spring_security_demo.securityConfig.JwtAuthenticationFilter;
import com.example.sr_spring_security_demo.securityConfig.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserServiceImp userServiceImp;
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final JwtUtil jwtUtil;
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getEmail(),
                        authenticationRequest.getPassword()
                )
        );
        var user = userRepository.findByEmail(authenticationRequest.getEmail());

        System.out.println("User information : " + user);
        var jwtToken = jwtUtil.generateToken(user);

        return AuthenticationResponse.builder().token(jwtToken).build();
    }


}
