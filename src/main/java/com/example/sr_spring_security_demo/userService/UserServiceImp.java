package com.example.sr_spring_security_demo.userService;

import com.example.sr_spring_security_demo.model.Role;
import com.example.sr_spring_security_demo.model.UserInfo;
import com.example.sr_spring_security_demo.model.UserRequest;
import com.example.sr_spring_security_demo.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;

@Service
//@AllArgsConstructor
public class UserServiceImp implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper = new ModelMapper();

    public UserServiceImp(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserInfo userInfo = userRepository.findByEmail(username);
        System.out.println(userInfo);
        if (userInfo == null){
            throw new UsernameNotFoundException("User not found");
        }

        return userInfo;
    }
//    public UserInfo addNewUser(UserRequest userRequest){
//        boolean isRole = false;
//        UserInfo userInfo = modelMapper.map(userRequest, UserInfo.class);
//        userInfo.setPassword(passwordEncoder.encode(userRequest.getPassword()));
//
//        for (Role role: Role.values()) {
//        if (userRequest.getRole().equalsIgnoreCase(role.name())){
//            isRole = true;
//            break;
//          }
//        }
//        if (!isRole){
//            throw new RuntimeException();
//        }
//
//       return userRepository.saveUser(userInfo);
//
//    }








}
