//package com.example.sr_spring_security_demo.securityConfig;
//
//import lombok.AllArgsConstructor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//import org.springframework.security.web.SecurityFilterChain;
//
//@Configuration
//@EnableWebSecurity
//@AllArgsConstructor
//public class InMemoryUserConfig {
//
//    private final BCryptPasswordEncoder passwordEncoder;
//    @Bean
//    public UserDetailsService userDetailsService(){
//        UserDetails user = User.withUsername("user")
//                .password(passwordEncoder.encode("1234"))
//                .roles("USER")
//                .build();
//
//        UserDetails admin = User.withUsername("admin")
//                .password(passwordEncoder.encode("0987"))
//                .roles("ADMIN")
//                .build();
//
//
//        return new InMemoryUserDetailsManager(user,admin);
//    }
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception{
//
//        httpSecurity.csrf().disable()
//                .authorizeHttpRequests(
//                        (req) -> req.requestMatchers(HttpMethod.GET,"/api/v1/users/user").hasAuthority("USER")
//                                .requestMatchers("/api/v1/users/admin").hasAuthority("ADMIN")
//                                .requestMatchers("/api/v1/users/user_admin").hasAnyAuthority("USER","ADMIN")
//                                .requestMatchers("/api/v1/Users/home").permitAll()
//                                .anyRequest()
//                                .authenticated()
//                ).formLogin().disable().httpBasic().authenticationEntryPoint((request, response, authException) -> {
//                    response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
//                });
//
//        return httpSecurity.build();
//    }
//
//}
