package com.example.sr_spring_security_demo.controller;

import com.example.sr_spring_security_demo.model.AuthenticationRequest;
import com.example.sr_spring_security_demo.model.AuthenticationResponse;
import com.example.sr_spring_security_demo.userService.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authentication(
            @RequestBody AuthenticationRequest authenticationRequest
            ){
        return ResponseEntity.ok(authenticationService.authenticate(authenticationRequest));
    }



}
