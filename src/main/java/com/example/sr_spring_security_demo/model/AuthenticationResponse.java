package com.example.sr_spring_security_demo.model;

import lombok.*;

@Data
//@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenticationResponse {
    private String token;
}
