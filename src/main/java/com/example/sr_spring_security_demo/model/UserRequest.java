package com.example.sr_spring_security_demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {
    private String name;
    private Integer age;
    private String gender;
    private String email;
    private String password;
    private String role;

}
